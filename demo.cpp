#include "Hogwarts.h"

BEGIN_GAME

    CREATE SPELL {
        NAME: "Sectumsempra",
        ACTION: START
            IF NOT(GET_HP(ATTACKER) < 30) DO
                HEAL(ATTACKER, 20)
            ELSE
                HEAL(ATTACKER, 10)
            END
        END
    }

    CREATE SPELLS [
        SPELL {
            NAME: "Expulso",
            ACTION: START
                DAMAGE(DEFENDER, 20)
            END
        },
        SPELL {
            NAME: "Anapneo",
            ACTION: START
                // Κάνει heal στον εαυτό του (ATTACKER)
                HEAL(ATTACKER, 30)
            END
        },
        SPELL {
            NAME: "Obliviate",
            ACTION: START
                EQUIP(DEFENDER,"_")
            END
        },
        SPELL {
            NAME: "ConfundusCharm",
            ACTION: START
                EQUIP(DEFENDER,"---α")
            END
        },
        SPELL {
            NAME: "Stupefy",
            ACTION: START
                IF GET_HP(ATTACKER) >= 30 DO
                    IF GET_HOUSE(ATTACKER) == "Gryffindor" DO
                        SHOW "YES" << endl;
                    ELSE
                        SHOW "NO" << endl;
                    END
                END
            END
        }
    ]

    CREATE SPELL {
        NAME: "DAMAGEAFTER",
        ACTION: START
            AFTER 2 ROUNDS DO 
                cout << "TORA EKTELEITAI" <<endl;
                DAMAGE(DEFENDER, 20) 
            END
        END
    }

    CREATE SPELL {
        NAME: "HEALLOOP",
        ACTION: START
            FOR 3 ROUNDS DO
                IF AND ( GET_HP(DEFENDER) > 10, GET_HP(DEFENDER) <= 100 ) DO
                    HEAL(DEFENDER, 10)
                END
            END
        END
    }

    CREATE WIZARD {
        NAME: "Gilbert Talpin",
        HOUSE: "Slytherin",
        HP: 100
    }

    CREATE WIZARDS [
        WIZARD{
            NAME: "Harry Potter",
            HOUSE: "Gryffindor",
            HP: 100
        },
        WIZARD{
            NAME: "Hermione Granger",
            HOUSE: "Gryffindor",
            HP: 200
        },
        WIZARD{
            NAME: "Luna Lovegood",
            HOUSE: "Ravenclaw",
            HP: 200
        },
        WIZARD{
            NAME: "Draco Malfoy",
            HOUSE: "Slytherin",
            HP: 100
        }
    ]

    //Spell sp1("sp1",[](Wizard w, int hp){cout << "dummy"<<endl;});
    //int actual_spell = lookupSpell("spell_name0");

    MR "Harry Potter" LEARN [
        SPELL_NAME(HEALLOOP)
        SPELL_NAME(Stupefy)
        SPELL_NAME(Expulso)
    ]

    MRS "Hermione Granger" LEARN [
        SPELL_NAME(Expulso)
        SPELL_NAME(Stupefy)
        SPELL_NAME(Obliviate)
        SPELL_NAME(ConfundusCharm)
    ]

    MRS "Luna Lovegood" LEARN [
        SPELL_NAME(Sectumsempra)
    ]

    MR "Draco Malfoy" LEARN [
        SPELL_NAME(Expulso)
    ]

    DUEL

/* 
    if( NOT ( AND ( GET_HP(DEFENDER) > 20, GET_HP(DEFENDER) < 70 )) ){
        cout << "TRUE" << endl;
    }

    IF GET_HP(DEFENDER) <= 20 DO
        cout << "1st" << endl;
    ELSE_IF GET_HP(DEFENDER) >= 580 DO
        cout << "2nd" << endl;
    ELSE
        cout << "3rd" << endl;
    END
*/

END_GAME