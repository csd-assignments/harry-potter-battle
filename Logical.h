#ifndef LOGICAL_H
#define LOGICAL_H

#include <iostream>
#include <vector>
using namespace std;

#define NOT(arg)    !(arg)

template<typename ... Args>
bool AND(Args... args){

    vector<bool> v = {args...};
    //cout << "size: "<< v.size() << endl;
    bool tmp = v[0];
    for (int i = 1; i < v.size(); i++) {
        tmp = (tmp && v[i]);
    }
    return tmp;
}

template<typename ... Args>
bool OR(Args... args){

    vector<bool> v = {args...};
    bool tmp = v[0];
    for (int i = 1; i < v.size(); i++) {
        tmp = (tmp || v[i]);
    }
    return tmp;
}

#endif