
In this project I constructed a new language syntax to simulate a Harry Potter battle.
The user/programmer of this language can create wizards and spells. Each spell has a specific action which will be rendered when a wizard uses them during a battle.
The language provides: logical operations, conditions, loops, prints.

Example of use:

CREATE SPELL {
NAME: "Anapneo",
ACTION : START
HEAL ATTACKER 25
END
}

CREATE SPELLS[
SPELL{
NAME: "Anapneo",
ACTION : START
HEAL ATTACKER 30
END
},
SPELL{
NAME: "Expulso",
ACTION: START
DAMAGE DEFENDER 22
END
}
]

A wizard may also learn a spell with this syntax:
MR/MRS "wizard name" LEARN [
SPELL_NAME(spell name)
SPELL_NAME(spell name)
...
]

Examples:

MR "Harry Potter" LEARN [
SPELL_NAME(Sectumsempra)
SPELL_NAME(Expelliarmus)
]
MRS "Hermione Granger" LEARN [
SPELL_NAME(WinguardiumLeviosa)
SPELL_NAME(ConfundusCharm)
SPELL_NAME(Stupefy)
SPELL_NAME(Obliviate)
]

Each wizard belongs to a House. Because of rivalry between Houses, wizards have developed some special powers to survive the battles... damage and healing is not the same for everyone.

The battle between two wizards is represented via console GUI with the command DUEL. The battle is over with the defeat of a wizard.
