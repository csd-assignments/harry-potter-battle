#ifndef HOGWARTS_H
#define HOGWARTS_H

#include "Wizard&Spell.h"
#include "Logical.h"

#define BEGIN_GAME int main(){
#define END_GAME ;return 0;}

#define CREATE ;
#define WIZARD Wizard
#define WIZARDS Wizard()

#define SPELL Spell
#define SPELLS Spell()

#define NAME (0)?"unknown"
#define HOUSE (0)?"unknown"
#define HP (0)?0

#define ACTION (0)?[](Wizard w, int hp){}
#define START [](Wizard w, int hp){
#define END ;}

#define DEFENDER defender
#define ATTACKER attacker

#define GET_HP(w)       w.getHealthPoints()
#define GET_HOUSE(w)    w.getHouse()
#define GET_NAME(w)     w.getName()
#define HAS_WAND(w)     w.has_wand()

#define DAMAGE(w,hp) damage(&w, hp);
#define HEAL(w,hp) heal(&w, hp);

#define EQUIP(w,str) equip(&w,str);

#define SHOW ;cout << endl <<

#define MR ;index = lookupWizard(
#define MRS ;index = lookupWizard(

#define LEARN ); Wizard()
#define SPELL_NAME(name) #name " "

#define IF  ;if(
#define DO  ){
#define ELSE_IF ;}else if(
#define ELSE ;}else{

#define FOR ;onlyfor = true; looptmp.push_back(make_pair(0,[](Wizard w, int hp){ }));\
            looptmp[looptmp.size()-1].first =

#define ROUNDS ;looptmp[looptmp.size()-1].second = [](Wizard w, int hp

#define AFTER ;onlyafter = true; looptmp.push_back(make_pair(0,[](Wizard w, int hp){ }));\
              looptmp[looptmp.size()-1].first =

#define PRINT_WIZARDS ;{ vector<Wizard>::iterator ptr;\
            for (ptr = wizards.begin(); ptr < wizards.end(); ptr++)\
                cout << ptr->getName() << endl;\
            }

#define PRINT_SPELLS ;{ vector<Spell>::iterator ptr;\
            for (ptr = spells.begin(); ptr < spells.end(); ptr++)\
                cout << ptr->getName() << endl;\
            }

#define DUEL ;duel();

Wizard attacker, defender;
bool heal_or_damage = false;
bool onlyfor = false, onlyafter = false, forafter = false;

int round = 1;

LoopVector attacker_forloop, defender_forloop;
LoopVector attacker_afterloop, defender_afterloop;

void CastSpell(Wizard w, int hp, function <void(Wizard,int)> Action){
    //cout <<"cast\n";
    Action(w,hp);
}

void executeFOR(){
 
    for (int i=0; i<looptmp.size(); i++){
        attacker_forloop.push_back(looptmp[i]);
    }

    //ekteleitai otan to attacker_forloop dn einai empty
    for (int i=0; i<attacker_forloop.size(); i++){

        int round_number = attacker_forloop[i].first;

        if(round_number == 0){
            attacker_forloop.erase(attacker_forloop.begin()+i);
        }
        cout << attacker_forloop[i].first << " "; 
        attacker_forloop[i].second(attacker, attacker.getHealthPoints());
        (attacker_forloop[i].first)--;
    }
    looptmp.clear();
}

void executeAFTER(){

    for (int i=0; i<looptmp.size(); i++){
        attacker_afterloop.push_back(looptmp[i]);
    }

    for (int i=0; i<attacker_afterloop.size(); i++){

        int round_number = attacker_afterloop[i].first;

        if(round_number == 0){
            attacker_afterloop[i].second(attacker, attacker.getHealthPoints());
            //attacker_afterloop.erase(attacker_afterloop.begin()+i);
        }
        cout << "left: " << round_number << endl;
        
        (attacker_afterloop[i].first)--;
    }
    looptmp.clear();
}

void damage(Wizard *w, int hp){

    int damage_hp, tmp, new_hp;

    if((*w).getHouse()=="Gryffindor"){

        if(attacker.getHouse()=="Slytherin"){

            tmp = hp*0.3; /* 30% ligotero damage */
            damage_hp = 1.2*hp /* 20% epipleon damage */-tmp;

        }else{
            tmp = hp*0.2; /* 20% ligotero damage */

            if(attacker.getHouse()=="Hufflepuff"){
                damage_hp = 1.07*hp /* 7% epipleon damage */-tmp;
            }
            else if(attacker.getHouse()=="Ravenclaw" && round%2 != 0){
                damage_hp = 1.07*hp /* 7% epipleon damage */-tmp;
            }else{
                damage_hp = hp-tmp;
            }
        }
    }else if((*w).getHouse()=="Slytherin"){

        if(attacker.getHouse()=="Hufflepuff"){
            damage_hp = 1.07*hp /* 7% epipleon damage */;
        }
        else if(attacker.getHouse()=="Ravenclaw"){

            if(round%2 != 0){ //perittos
                damage_hp = 1.07*hp /* 7% epipleon damage */;
            }else{ //artios
                damage_hp = hp;
            }
        }else if(attacker.getHouse()=="Slytherin"){
            damage_hp = 1.15*hp;
        }else{
            damage_hp = hp;
        }
    }else if((*w).getHouse()=="Hufflepuff"){
        tmp = hp;
        if(attacker.getHouse()=="Slytherin"){
            tmp = 1.15*hp;
        }else if(attacker.getHouse()=="Hufflepuff"){
            tmp = tmp+0.7*hp;
        }else if(attacker.getHouse()=="Ravenclaw"){

            if(round%2 != 0){ //perittos
                damage_hp = 1.07*hp /* 7% epipleon damage */;
            }else{ //artios
                damage_hp = hp-tmp;
            }
        }else{
            damage_hp = tmp-0.07*hp;
        }
    }
    else if((*w).getHouse()=="Ravenclaw"){
        tmp = hp;
        if(attacker.getHouse()=="Slytherin"){
            tmp = 1.15*hp;
        }else if(attacker.getHouse()=="Hufflepuff"){
            tmp = tmp+0.7*hp;
        }else if(attacker.getHouse()=="Ravenclaw"){

            if(round%2 != 0){ //perittos
                damage_hp = 1.07*hp /* 7% epipleon damage */;
            }else{ //artios
                damage_hp = hp-tmp;
            }
        }else{
            damage_hp = hp;
        }
    }
    new_hp = (*w).getHealthPoints()-damage_hp;
    (*w).setHealthPoints(new_hp);
}

void heal(Wizard *w, int hp){
    int new_hp = (*w).getHealthPoints()+hp;
    (*w).setHealthPoints(new_hp);
}

void equip(Wizard *w, string str){

    if(w == &defender){
        if(str == "---α") defender.equip();
        else if(str == "_") defender.disarm();

    }else{
        if(str == "---α") attacker.equip();
        else if(str == "_") attacker.disarm();
    }
}

void PrintVectorElements(vector<Spell> sv){

    vector<Spell>::iterator ptr;
    for (ptr = sv.begin(); ptr < sv.end(); ptr++){
        cout << ptr->getName() << endl;
    }
}

void PrintWizardState(){

    cout << endl;
    cout << "#######################" << endl;
    cout << "Name: "<< attacker.getName() << endl;
    cout << "HP: "<< attacker.getHealthPoints() << endl;
    if(attacker.has_wand()){
        cout << "Wand equipped" << endl;
    }else{
        cout << "Wand not equipped" << endl;
    }
    cout << "#######################" << endl << endl;

    cout << "#######################" << endl;
    cout << "Name: "<< defender.getName() << endl;
    cout << "HP: "<< defender.getHealthPoints() << endl;
    if(defender.has_wand()){
        cout << "Wand equipped" << endl;
    }else{
        cout << "Wand not equipped" << endl;
    }
    cout << "#######################" << endl;
}

void DuelRound(){

    string spell_name;

    if(round%2 == 0){
        if(attacker.getHouse()=="Ravenclaw"){
            heal(&attacker, attacker.getHealthPoints()*0.05);
        }
        if(defender.getHouse()=="Ravenclaw"){
            heal(&defender, defender.getHealthPoints()*0.05);
        }
    }
    if(!attacker.has_wand()){
        cout << endl << "\033[32mPlayer (\033[0m"<< attacker.getName();
        cout << "\033[32m) does not have a wand: unable to cast a spell\033[0m\n";
        swap(attacker, defender);
        swap(attacker_forloop, defender_forloop);
        swap(attacker_afterloop, defender_afterloop);
    }

    vector<Spell> sv = attacker.getWizardSpells();

    if(sv.empty()){
        cout << "\033[32mPlayer (\033[0m"<< attacker.getName() << "\033[32m) has not learnt any spells\033[0m\n";
    }else{
        do {
            cout << endl << "\033[32mPlayer (\033[0m"<< attacker.getName() << "\033[32m) select spell:\033[0m\n";
            cout << "\033[32m-----------------------\033[0m\n";
            //print player's available spells
            PrintVectorElements(sv);
            cout << "\033[32m-----------------------\033[0m\n";

            getline(cin, spell_name);

        } while(lookupSpell(spells, spell_name) == -1);

        int i = lookupSpell(sv, spell_name);
        // sv[i] --> to spell
        CastSpell(attacker, attacker.getHealthPoints(), sv[i].getSpellAction());

        if(onlyfor){ executeFOR(); }
        if(onlyafter){ executeAFTER(); }
        //if(forafter){}

        PrintWizardState();
    }
}

void duel(){

    string player1, player2;

    cout << "------------------HARRY POTTER THE GAME------------------" << endl;

SelectPlayer1:
    //cout << "\033[1;32m (Player1) select spell:\033[0m\n";
    cout << endl << "\033[32mPlayer1 select wizard:\033[0m\n";
    cout << "\033[32m-----------------------\033[0m\n";
    PRINT_WIZARDS
    cout << "\033[32m-----------------------\033[0m\n";

    getline(cin, player1);

    if(lookupWizard(player1) == -1){
        goto SelectPlayer1;
    } // or do {} while()

SelectPlayer2:
    cout << endl << "\033[32mPlayer2 select wizard:\033[0m\n";
    cout << "\033[32m-----------------------\033[0m\n";
    PRINT_WIZARDS
    cout << "\033[32m-----------------------\033[0m\n";

    getline(cin, player2);

    if(lookupWizard(player2) == -1){
        goto SelectPlayer2;
    }
/* ------------- THE GAME STARTS ------------- */

    attacker = wizards[lookupWizard(player1)];
    defender = wizards[lookupWizard(player2)];
    
    while ( (attacker.getHealthPoints() > 0) && (defender.getHealthPoints() > 0))
    {
        cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" <<endl;
        cout << "Round "<< round <<endl;
        cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" <<endl;

        DuelRound();
        swap(attacker, defender);
        swap(attacker_forloop, defender_forloop);
        swap(attacker_afterloop, defender_afterloop);
        round++;
    }
    cout << endl << "\033[1mEND OF GAME\033[0m\n";

    if(defender.getHealthPoints()<=0){
        cout << "\033[36mWinner: \033[0m" << attacker.getName()<< endl;
    }else{
        cout << "\033[36mWinner: \033[0m" << defender.getName()<< endl;
    }
}
#endif
