#ifndef WIZARD_H
#define WIZARD_H

#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

using namespace std;

class Wizard;
vector<Wizard> wizards; // oloi oi wizards

class Spell;
vector<Spell> spells; // ola ta spells

int index/*index to wizards vector*/;

typedef vector<pair<int, function <void(Wizard,int)> >> LoopVector;
LoopVector looptmp;

class Spell {
    private:
        string name;
        function <void(Wizard,int)> spell_action;
        /* lambda pou pairnei orismata Wizard kai 
            int kai epistrefei void */
    public:
        Spell(){}
        Spell(string n, function <void(Wizard,int)> sp_act){
            this->name = n;
            this->spell_action = sp_act;
            spells.push_back(*this);
        }
        void setName(string n){this->name = n;}
        string getName(){return this->name;}

        void setSpellAction(function <void(Wizard,int)> sp_act){
            this->spell_action = sp_act;
        }
        function <void(Wizard,int)> getSpellAction(){
            return this->spell_action;
        }

        int operator[](Spell sp){
            //cout << "Spell is " << sp.getName() << endl;
            return 1;
        }
};

int lookupSpell(vector<Spell> sv, string name){
    //cout << "lookup spell" << endl;
    for(int i = 0;i< sv.size();i++){
        if(sv[i].getName() == name){
            return i;
        }
    }
    cout << "\033[1;31mThere is no such spell!\033[0m\n" << endl;
    return -1;
}

class Wizard {
    private:
        string name;
        string house;
        int hp;
        bool wand = true;
        vector<Spell> WizardSpells; // ta spells pou exei mathei
    public:

        Wizard(){
            this->name = "TEST WIZARD";
            this->house = "TEST HOUSE";
            this->hp = 100;
        }
        Wizard(string n, string h, int hp){
            this->name = n;
            this->house = h;
            this->hp = hp;
            wizards.push_back(*this);
        }

        void setName(string n){this->name = n;}
        string getName(){return this->name;}

        void setHouse(string h){this->house = h;}
        string getHouse(){return this->house;}

        void setHealthPoints(int hp){this->hp = hp;}        
        int getHealthPoints(){return this->hp;}

        void equip(){ this->wand = true; }
        void disarm(){ this->wand = false; }

        bool has_wand(){
            return (this->wand == true);
        }

        int learnSpell(Spell s){
            this->WizardSpells.push_back(s);
            //cout << "LEARNT SPELL" << endl;
            return 0;
        }

        vector<Spell> getWizardSpells(){
            return this->WizardSpells;
        }

        int operator[](string str){ 

            string token, delimiter = " ";
            size_t pos = 0;

            while ((pos = str.find(delimiter)) != string::npos) {
                /* every token is a separate spell */
                token = str.substr(0, pos);

                wizards[index].learnSpell(spells[lookupSpell(spells,token)]);
                str.erase(0, pos + delimiter.length());
            }
            /*str is now empty...*/
            return 1;
        }

        int operator[](Wizard w){
            //cout << "Wizard is " << w.getName() << endl;
            return 1;
        }
};

int lookupWizard(string name){
    //cout << "lookup wizard" << endl;
    for(int i = 0;i< wizards.size();i++){
        if(wizards[i].getName() == name){
            return i;
        }
    }
    cout << "\033[1;31mThere is no such wizard!\033[0m\n";
    return -1;
}

/* overload comma for wizards */
Wizard operator,(Wizard w1, Wizard w2)
{
    Wizard tmp;

    tmp.setName(w2.getName());
    tmp.setHouse(w2.getHouse());
    tmp.setHealthPoints(w2.getHealthPoints());

    return tmp;
}

/* overload comma for spells */
Spell operator,(Spell sp1, Spell sp2){ return sp2; }

#endif